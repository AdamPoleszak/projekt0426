import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Random;

public class Registration extends TestBase {
    @Test
    public void testingMethods() {
        driver.get("http://toolsqa.com/automation-practice-form/");
        inputNamesDate();
        pickSexExpRandomRadio();
        findAndPick2checkboxes();
        sendPictureProfile();
        selectContinentSeleniumCommands();
    }

    public void findElementAndSendKeysBy(By bySelector, String sentText) {
        driver.findElement(bySelector).sendKeys(sentText);

    }

    public void inputNamesDate() {
        findElementAndSendKeysBy(By.cssSelector("[name=\"firstname\"]"), "Adam");
        findElementAndSendKeysBy(By.cssSelector("[name=\"lastname\"]"), "Poleszak");
        findElementAndSendKeysBy(By.id("datepicker"), "12.09.1990");
    }

    public void pickSexExpRandomRadio() {
        findElementsMakeListPickRandom(By.cssSelector("input[name=\"sex\"]"));
        findElementsMakeListPickRandom(By.cssSelector("input[name=\"exp\"]"));
    }

    public void findElementsMakeListPickRandom(By uniqueSelector) {
        List<WebElement> list = driver.findElements(uniqueSelector);
        WebElement element = list.get(randomListIndex(list));
        System.out.println("Wybrano element: " + element.getAttribute("value"));
        element.click();
    }

    public int randomListIndex(List<WebElement> list1) {
        Random random = new Random();
        return random.nextInt(list1.size() - 1);
    }

    public void findAndPick2checkboxes() {
        List<WebElement> list = driver.findElements(By.cssSelector("input[name=\"tool\"]"));
        list.get(0).click();
        list.get(1).click();
        Assert.assertFalse(list.get(2).isSelected());
        Assert.assertTrue(list.get(0).isSelected());
        Assert.assertTrue(list.get(1).isSelected());
        for (WebElement element : list)
            if (element.isSelected()) {
                System.out.println("Element listy " + element.getAttribute("value") + " is selected");
            }
            else {
                System.out.println("Element listy "+ element.getAttribute("value") + " is not selected");
            }
    }
    public void sendPictureProfile() {
        driver.findElement(By.className("input-file")).sendKeys("c:\\Users\\madas\\Desktop\\Tapety\\" +
                "Przechwytywanie.jpg");
    }
    public void selectContinentSeleniumCommands(){
        Select select = new Select(driver.findElement(By.cssSelector("#continents")));
        select.selectByVisibleText("Europe");
        Assert.assertEquals("Europe",select.getFirstSelectedOption().getText());
        Select select2 = new Select(driver.findElement(By.cssSelector("#selenium_commands")));
        select2.selectByIndex(0);
        System.out.println("Zaznaczono" + select2.getFirstSelectedOption().getText());
    }
}
